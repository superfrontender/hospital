import Swiper from 'swiper';

window.Swiper = Swiper;

var advantagesSlider;

window.onload = () => {
    const homeSlider = new Swiper('.slider-home', {
        loop: true,
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.slider-home__nav_next',
            prevEl: '.slider-home__nav_prev'
        }
    });

    const directionsSlider = new Swiper('.direction-slider', {
        scrollbar: {
            el: '.direction-slider__scrollbar',
            dragSize: 80,
        },
        slidesPerView: 'auto',
        spaceBetween: 30,
    });

    const feedbacksSlider = new Swiper('.feedbacks-slider', {
        allowTouchMove: false,
        loop: true,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.feedbacks-slide-nav__number',
            type: 'custom',
            renderCustom: function (swiper, current, total) {
                return '<span class="feedbacks-slide-nav-number__current">' + current + '</span><span class="feedbacks-slide-nav-number__total">/' + total + '</span>';
            },
        },
        navigation: {
            nextEl: '.feedbacks-slide-nav__arrow_left',
            prevEl: '.feedbacks-slide-nav__arrow_right'
        }
    });

    let windowWidth = window.innerWidth;
    advantagesSliderDestroyInit(windowWidth);
}

window.addEventListener('resize', (event) => {
    let windowWidth = window.innerWidth;
    advantagesSliderDestroyInit(windowWidth);
});

function advantagesSliderDestroyInit(windowWidth) {
    console.log(windowWidth);
    if (windowWidth <= 982) {
        console.log('destroy');
        advantagesSlider.destroy();  
    } else {
        initAdvantagesSlider();
    }
}

function initAdvantagesSlider() {
    advantagesSlider = new Swiper('.advantages-slider', {
        slidesPerView: 'auto',
        spaceBetween: 30,
        navigation: {
            prevEl: '.advantages-slider-nav__left',
            nextEl: '.advantages-slider-nav__right'
        }
    }); 
}